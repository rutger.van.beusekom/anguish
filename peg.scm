(define-module (peg)
  #:use-module (peg codegen)
  #:use-module (peg string-peg)
  ;; Note: the most important effect of using string-peg is not whatever
  ;; functions it exports, but the fact that it adds a new handler to
  ;; peg-sexp-compile.
  #:use-module (peg simplify-tree)
  #:use-module (peg using-parsers)
  #:use-module (peg cache)

  #:re-export (define-peg-pattern
               define-peg-string-patterns
               define-skip-parser
               %peg:debug?
               %peg:fall-back?
               %peg:locations?
               %peg:skip?
               %peg:error
               match-pattern
               search-for-pattern
               compile-peg-pattern
               keyword-flatten
               context-flatten
               peg:start
               peg:end
               peg:string
               peg:tree
               peg:substring
               peg-record?))
