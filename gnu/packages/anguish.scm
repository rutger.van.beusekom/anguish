(define-module (gnu packages anguish)
  #:use-module (guix build-system gnu)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages man)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages texinfo))

(define %source-dir (getcwd))

(define-public anguish
  (package
    (name "anguish")
    (version "0.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://gitlab.com/rutger.van.beusekom/anguish"))
       (sha256
        (base32 "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"))))
    (inputs `(("guile" ,guile-3.0-latest)))
     ; for guix environment -l guix.scm
    (build-system gnu-build-system)
    (arguments
     `(#:modules ((ice-9 popen)
                  ,@%gnu-build-system-modules)
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'setenv
           (lambda _
             (setenv "GUILE_AUTO_COMPILE" "0")))
         (add-after 'install 'wrap-binaries
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (guile (assoc-ref %build-inputs "guile"))                    
                    (effective (read
                                (open-pipe* OPEN_READ
                                            "guile" "-c"
                                            "(write (effective-version))"))))
               #t))))))
    (synopsis "ANGUISH ANother GUIle SHell")
    (description "Guile based shell replacement")
    (home-page "https://gitlab.com/rutger.van.beusekom/anguish")
    (license license:gpl3+)))

