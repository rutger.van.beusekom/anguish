;;  ANGUISH is ANother GUIle SHell

;;  Copyright (C) 2021  Rutger van Beusekom

;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU Affero General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU Affero General Public License for more details.

;;  You should have received a copy of the GNU Affero General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (grammar)
  #:use-module (peg)
  
  #:re-export (%peg:locations?
               %peg:skip?
               %peg:fall-back?
               %peg:debug?
               %peg:error)

  #:export (peg:parse
            peg:skip-parse))

(define-skip-parser peg-ws none (or " " "\t"))
(define-skip-parser peg-line all (and "#" (* (and (not-followed-by "\n") peg-any)) "\n"))
(define-skip-parser peg-skip all (* (or peg-ws peg-line)))

(define peg:skip-parse peg-skip)

(define* (peg:parse string)
  
  (define-peg-string-patterns
    "program      <-- compound? linebreak

compound          <-  linebreak (pipeline ((AND-IF / OR-IF) pipeline)* separator)+

separator         <   ('&' / ';') linebreak / newlines / EOF

sequential-sep    <   SEMICOLON linebreak / newlines

linebreak         <   '\n'*

newlines          <   '\n'+

pipeline          <-- BANG? command (PIPE linebreak command)*

command           <-- !KEYWORD simple-command
                    / compound-command io-redirect*
                    / function

compound-command  <-- brace-group
                    / subshell
                    / for-clause
                    / case-clause
                    / if-clause
                    / while-clause
                    / until-clause

brace-group       <-- BRACE-OPEN compound BRACE-CLOSE#

subshell          <-- PAREN-OPEN compound PAREN-CLOSE#

for-clause        <-- FOR name# ((linebreak IN (!KEYWORD word)*)? sequential-sep)? do-group#

case-clause       <-- CASE word# linebreak IN# linebreak case-item* ESAC#

case-item         <-- PAREN-OPEN? pattern# PAREN-CLOSE# (linebreak / compound) (DSEMI linebreak)?

pattern           <-- word (PIPE word)*

if-clause         <-- IF compound# THEN# compound# else-part? FI#

else-part         <-- ELIF compound# THEN# compound# else-part?
                    / ELSE compound#

while-clause      <-- WHILE compound# do-group#

until-clause      <-- UNTIL compound# do-group#

do-group          <-- DO compound# DONE#

function          <-- name PAREN-OPEN PAREN-CLOSE# linebreak compound-command# io-redirect*

simple-command    <-- (assignment / io-redirect)+
                    / (word / io-redirect)+

assignment        <-- name ASSIGN (word / io-redirect)+

word              <-- single-quoted / double-quoted / substitution / expansion / WORD

name              <-- NAME

io-redirect       <-- IO-NUMBER? io-file / IO-NUMBER? io-here

io-file           <-- (LESS / LESS-AND / GREATER / GREATER-AND / SHIFT-RIGHT / LESS-GREATER / CLOBBER) filename

io-here           <-- (SHIFT-LEFT / SHIFT-LEFT-DASH) word

filename          <-- word

single-quoted     <-- SINGLE-QUOTE (!SINGLE-QUOTE .)* SINGLE-QUOTE

double-quoted     <-- DOUBLE-QUOTE (!DOUBLE-QUOTE (expansion / substitution / .))* DOUBLE-QUOTE

expansion         <-- DOLLAR BRACE-OPEN parameter COLON? (DASH / ASSIGN / QUESTION / PLUS) word BRACE-CLOSE#
                    / DOLLAR BRACE-OPEN parameter PERCENT word? BRACE-CLOSE#
                    / DOLLAR BRACE-OPEN parameter BANG BANG word? BRACE-CLOSE#
                    / DOLLAR BRACE-OPEN parameter name BANG word? BRACE-CLOSE#
                    / DOLLAR BRACE-OPEN BANG? name BRACE-CLOSE#
                    / DOLLAR PAREN-OPEN PAREN-OPEN expression PAREN-CLOSE# PAREN-CLOSE#
                    / DOLLAR parameter

expression        <-  'TODO'

parameter         <-- name / BANG / AT / ASTERISK

substitution      <-- BACKTICK pipeline# BACKTICK# / DOLLAR PAREN-OPEN pipeline# PAREN-CLOSE#


AND-IF            <-  '&&'
ASSIGN            <   '='
ASTERISK          <   '*'
AT                <   '@'
BACKTICK          <   '`'
BRACE-OPEN        <   '{'
BRACE-CLOSE       <   '}'
BANG              <    '!' 
CLOBBER           <   '>|'  
COLON             <   ':'
DASH              <   '-'
DOUBLE-QUOTE      <   '\"'
DOLLAR            <   '$'
DSEMI             <   ';;'
EOF               <   !.
GREATER           <   '>'
GREATER-AND       <   '>&'
IO-NUMBER         <-  [0-9]+
LESS              <   '<'
LESS-AND          <   '<&'
LESS-GREATER      <   '<>'
NAME              <-  !KEYWORD [a-zA-Z_][a-zA-Z_0-9]*
PIPE              <   '|'
SHIFT-LEFT        <   '<<'
SHIFT-LEFT-DASH   <   '<<-'
SHIFT-RIGHT       <   '>>'
OR-IF             <   '||'
PAREN-OPEN        <   '('
PAREN-CLOSE       <   ')'
PERCENT           <   '%'
PLUS              <   '+'
QUESTION          <   '?'
SEMICOLON         <   ';'
SINGLE-QUOTE      <   [']
WORD              <-  (!expansion ![ \n{}()|;] .)+

CASE              <   'case'  ![a-zA-Z0-9_]
DO                <   'do'    ![a-zA-Z0-9_]
DONE              <   'done'  ![a-zA-Z0-9_]
ELIF              <   'elif'  ![a-zA-Z0-9_]
ELSE              <   'else'  ![a-zA-Z0-9_]
ESAC              <   'esac'  ![a-zA-Z0-9_]
FI                <   'fi'    ![a-zA-Z0-9_]
FOR               <   'for'   ![a-zA-Z0-9_]
IF                <   'if'    ![a-zA-Z0-9_]
IN                <   'in'    ![a-zA-Z0-9_]
THEN              <   'then'  ![a-zA-Z0-9_]
UNTIL             <   'until' ![a-zA-Z0-9_]
WHILE             <   'while' ![a-zA-Z0-9_]

KEYWORD           <   CASE / DO / DONE / ELIF / ELSE / ESAC / FI / FOR / IF / IN / THEN / UNTIL / WHILE")

  (peg:tree (match-pattern program string)))

;; The PEG grammar above was derived from the
;; POSIX shell BNF grammar found at
;; https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html
;;
;; /* -------------------------------------------------------
;;    The grammar symbols
;;    ------------------------------------------------------- */
;; %token  WORD
;; %token  ASSIGNMENT_WORD
;; %token  NAME
;; %token  NEWLINE
;; %token  IO_NUMBER


;; /* The following are the operators (see XBD Operator)
;;    containing more than one character. */



;; %token  AND_IF    OR_IF    DSEMI
;; /*      '&&'      '||'     ';;'    */


;; %token  DLESS  DGREAT  LESSAND  GREATAND  LESSGREAT  DLESSDASH
;; /*      '<<'   '>>'    '<&'     '>&'      '<>'       '<<-'   */


;; %token  CLOBBER
;; /*      '>|'   */


;; /* The following are the reserved words. */


;; %token  If    Then    Else    Elif    Fi    Do    Done
;; /*      'if'  'then'  'else'  'elif'  'fi'  'do'  'done'   */


;; %token  Case    Esac    While    Until    For
;; /*      'case'  'esac'  'while'  'until'  'for'   */


;; /* These are reserved words, not operator tokens, and are
;;    recognized when reserved words are recognized. */


;; %token  Lbrace    Rbrace    Bang
;; /*      '{'       '}'       '!'   */


;; %token  In
;; /*      'in'   */


;; /* -------------------------------------------------------
;;    The Grammar
;;    ------------------------------------------------------- */
;; %start program
;; %%
;; program          : linebreak complete_commands linebreak
;;                  | linebreak
;;                  ;
;; complete_commands: complete_commands newline_list complete_command
;;                  |                                complete_command
;;                  ;
;; complete_command : list separator_op
;;                  | list
;;                  ;
;; list             : list separator_op and_or
;;                  |                   and_or
;;                  ;
;; and_or           :                         pipeline
;;                  | and_or AND_IF linebreak pipeline
;;                  | and_or OR_IF  linebreak pipeline
;;                  ;
;; pipeline         :      pipe_sequence
;;                  | Bang pipe_sequence
;;                  ;
;; pipe_sequence    :                             command
;;                  | pipe_sequence '|' linebreak command
;;                  ;
;; command          : simple_command
;;                  | compound_command
;;                  | compound_command redirect_list
;;                  | function_definition
;;                  ;
;; compound_command : brace_group
;;                  | subshell
;;                  | for_clause
;;                  | case_clause
;;                  | if_clause
;;                  | while_clause
;;                  | until_clause
;;                  ;
;; subshell         : '(' compound_list ')'
;;                  ;
;; compound_list    : linebreak term
;;                  | linebreak term separator
;;                  ;
;; term             : term separator and_or
;;                  |                and_or
;;                  ;
;; for_clause       : For name                                      do_group
;;                  | For name                       sequential_sep do_group
;;                  | For name linebreak in          sequential_sep do_group
;;                  | For name linebreak in wordlist sequential_sep do_group
;;                  ;
;; name             : NAME                     /* Apply rule 5 */
;;                  ;
;; in               : In                       /* Apply rule 6 */
;;                  ;
;; wordlist         : wordlist WORD
;;                  |          WORD
;;                  ;
;; case_clause      : Case WORD linebreak in linebreak case_list    Esac
;;                  | Case WORD linebreak in linebreak case_list_ns Esac
;;                  | Case WORD linebreak in linebreak              Esac
;;                  ;
;; case_list_ns     : case_list case_item_ns
;;                  |           case_item_ns
;;                  ;
;; case_list        : case_list case_item
;;                  |           case_item
;;                  ;
;; case_item_ns     :     pattern ')' linebreak
;;                  |     pattern ')' compound_list
;;                  | '(' pattern ')' linebreak
;;                  | '(' pattern ')' compound_list
;;                  ;
;; case_item        :     pattern ')' linebreak     DSEMI linebreak
;;                  |     pattern ')' compound_list DSEMI linebreak
;;                  | '(' pattern ')' linebreak     DSEMI linebreak
;;                  | '(' pattern ')' compound_list DSEMI linebreak
;;                  ;
;; pattern          :             WORD         /* Apply rule 4 */
;;                  | pattern '|' WORD         /* Do not apply rule 4 */
;;                  ;
;; if_clause        : If compound_list Then compound_list else_part Fi
;;                  | If compound_list Then compound_list           Fi
;;                  ;
;; else_part        : Elif compound_list Then compound_list
;;                  | Elif compound_list Then compound_list else_part
;;                  | Else compound_list
;;                  ;
;; while_clause     : While compound_list do_group
;;                  ;
;; until_clause     : Until compound_list do_group
;;                  ;
;; function_definition : fname '(' ')' linebreak function_body
;;                  ;
;; function_body    : compound_command                /* Apply rule 9 */
;;                  | compound_command redirect_list  /* Apply rule 9 */
;;                  ;
;; fname            : NAME                            /* Apply rule 8 */
;;                  ;
;; brace_group      : Lbrace compound_list Rbrace
;;                  ;
;; do_group         : Do compound_list Done           /* Apply rule 6 */
;;                  ;
;; simple_command   : cmd_prefix cmd_word cmd_suffix
;;                  | cmd_prefix cmd_word
;;                  | cmd_prefix
;;                  | cmd_name cmd_suffix
;;                  | cmd_name
;;                  ;
;; cmd_name         : WORD                   /* Apply rule 7a */
;;                  ;
;; cmd_word         : WORD                   /* Apply rule 7b */
;;                  ;
;; cmd_prefix       :            io_redirect
;;                  | cmd_prefix io_redirect
;;                  |            ASSIGNMENT_WORD
;;                  | cmd_prefix ASSIGNMENT_WORD
;;                  ;
;; cmd_suffix       :            io_redirect
;;                  | cmd_suffix io_redirect
;;                  |            WORD
;;                  | cmd_suffix WORD
;;                  ;
;; redirect_list    :               io_redirect
;;                  | redirect_list io_redirect
;;                  ;
;; io_redirect      :           io_file
;;                  | IO_NUMBER io_file
;;                  |           io_here
;;                  | IO_NUMBER io_here
;;                  ;
;; io_file          : '<'       filename
;;                  | LESSAND   filename
;;                  | '>'       filename
;;                  | GREATAND  filename
;;                  | DGREAT    filename
;;                  | LESSGREAT filename
;;                  | CLOBBER   filename
;;                  ;
;; filename         : WORD                      /* Apply rule 2 */
;;                  ;
;; io_here          : DLESS     here_end
;;                  | DLESSDASH here_end
;;                  ;
;; here_end         : WORD                      /* Apply rule 3 */
;;                  ;
;; newline_list     :              NEWLINE
;;                  | newline_list NEWLINE
;;                  ;
;; linebreak        : newline_list
;;                  | /* empty */
;;                  ;
;; separator_op     : '&'
;;                  | ';'
;;                  ;
;; separator        : separator_op linebreak
;;                  | newline_list
;;                  ;
;; sequential_sep   : ';' linebreak
;;                  | newline_list
;;                  ;
