(use-modules (guix gexp)
             (guix git-download)
             (guix packages)
             (gnu packages))

(define %source-dir (dirname (current-filename)))
(add-to-load-path %source-dir)
(%patch-path (cons %source-dir (%patch-path)))

(use-modules (gnu packages anguish))

(define-public anguish.git
  (package
    (inherit anguish)
    (version "git")
    (source (local-file %source-dir
                        #:recursive? #t
                        #:select? (git-predicate %source-dir)))))

anguish.git
