;;  ANGUISH is ANother GUIle SHell

;;  Copyright (C) 2021  Rutger van Beusekom

;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU Affero General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU Affero General Public License for more details.

;;  You should have received a copy of the GNU Affero General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (anguish)  
  #:use-module (ice-9 curried-definitions)
  #:use-module (ice-9 getopt-long)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 readline)
  #:use-module (ice-9 rdelim)

  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  
  #:use-module (grammar)

  #:export (main))

(define (transform program)
  (define ($ o)
    (match o
      (('program pipeline ...) (map $ o))
      (('pipeline command) `(pipeline ,@($ command)))
      (('pipeline command commands) `(pipeline ,@($ command) ,@($ commands)))
      ((('command command) ...) (map $ command))
      (('command command) `(,($ command)))
      (('simple-command word) `(list ,($ word)))
      (('simple-command word ...) `(list ,@(map $ word)))
      (('compound-command command) ($ command))
      (('for-clause name (word ...) do-group) `(for-each (lambda (,($ name)) ,($ do-group)) (list ,@(map $ word))))
      (('for-clause name word do-group) `(for ,($ name) ,($ word) ,($ do-group)))
      (('do-group command) ($ command))
      (('do-group commands ...) (map $ commands))
      (('word value) ($ value))
      (('assignment name value) `(assignment ,name ,(map $ value)))
      (('expansion parameter) ($ parameter))
      (('parameter name) `(parameter ,($ name)))
      (('name identifier) (string->symbol identifier))
      ((? string?) o)
      (_ o)))
  ($ program))

(define (peg:line-number string pos)
  (1+ (string-count string #\newline 0 pos)))

(define (peg:column-number string pos)
  (- pos (or (string-rindex string #\newline 0 pos) -1)))

(define (peg:line string pos)
  (let ((start (1+ (or (string-rindex string #\newline 0 pos) -1)))
        (end (or (string-index string #\newline pos) (string-length string))))
    (substring string start end)))

(define (error->string error)
  (let ((keywords '((CASE . "")
                    (DO . "do")
                    (DONE . "done")
                    (ELIF . "elif")
                    (ELSE . "else")
                    (ESAC . "esac")
                    (FI . "fi")
                    (FOR . "for")
                    (IF . "if")
                    (IN . "in")
                    (THEN . "then")
                    (UNTIL . "until")
                    (WHILE . "while"))))
    (or (assq-ref keywords error) error)))

(define (peg:message file-name string args)
  (let* ((pos (caar args))
         (expected (cadar args))
         (line-number (peg:line-number string pos))
         (column-number (peg:column-number string pos))
         (line (peg:line string pos))
         (indent (make-string (1- column-number) #\space)))
    (format (current-error-port) "~a:~a:~a: error\n~a\n~a^\n~a`~a' expected\n"
            file-name line-number column-number
            line indent indent (error->string expected))))

(define ((sh-parse debug?) string)
  (parameterize ((%peg:debug? debug?)
                 (%peg:skip? peg:skip-parse))
    (peg:parse (string-trim-right string))))

(define sh-prompt
  (let* ((l (string #\001))
         (r (string #\002))
         (e (string #\033))
         (user (getenv "USER"))
         (host (gethostname))
         (home (getenv "HOME")))
    (lambda ()
      (let* ((cwd (getcwd))
             (cwd (if (string-prefix? home cwd)
                      (string-replace cwd "~" 0 (string-length home))
                      cwd)))
        (string-append
         l e "[01;32m" r user "@" host l e "[00m" r ":"
         l e "[01;34m" r cwd l e "[00m" r (if (zero? (getuid)) "# " "$ "))))))

(define (sh-repl sh-parse sh-eval)      
      (let loop ((line (readline (sh-prompt))))
        (when (not (eof-object? line))
          (let* ((tree (false-if-exception (sh-parse line))))
            (when tree
              (sh-eval (transform tree)))
            (loop (let ((previous (if tree "" (string-append line "\n")))
                        (next (readline (if tree (sh-prompt) "> "))))
                    (if (eof-object? next) next
                        (string-append previous next))))))))

(define sh-eval pretty-print)

(define (main args)
  (let* ((option-spec '((help (single-char #\h) (value #f))
                        (debug (single-char #\d) (value #f))
                        (tree (value #f))))
         (options (getopt-long args option-spec #:stop-at-first-non-option #t))
         (debug? (option-ref options 'debug #f))
         (transform (if (option-ref options 'tree #f) identity transform))
         (files (option-ref options '() '())))
    
    (cond ((option-ref options 'help #f)
           (format (current-error-port)
                   "\
Usage: anguish [option] [program]

  -d, --debug         debug the PEG parser
  -h, --help          display this help and exit
      --tree          use the unmodified tree
"))
          ((null? files)
           (sh-repl (sh-parse debug?) sh-eval))
          (else
           (map (lambda (file-name)
                  (let ((string (with-input-from-file file-name read-string)))
                    (catch 'syntax-error
                      (cute (compose sh-eval transform (sh-parse debug?)) string)
                      (lambda (key . args)
                        (peg:message file-name string args)))))
                files)))))

